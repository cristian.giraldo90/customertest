// En tu componente React
import React, { useEffect } from 'react';

function OpenInNewWindowModal() {
  useEffect(() => {
    // Función para abrir la ventana modal
    const openModal = () => {
      // URL del sitio web que deseas cargar
      const url = 'https://mi.tigo.com.co/';
      
      // Opciones de la ventana modal
      const width = 800;
      const height = 450;
      const left = (window.innerWidth - width) / 2;
      const top = (window.innerHeight - height) / 2;
  
      // Abre una nueva ventana del navegador con dimensiones específicas
      window.open(url, '_blank', `noopener,noreferrer,width=${width},height=${height},left=${left},top=${top}`);
    };

    // Llama a la función openModal cuando el componente se monta
    openModal();
  }, []); // El segundo argumento [] asegura que se ejecute solo una vez al montar el componente

  // Este componente no renderiza nada visible en la interfaz, ya que la ventana modal se abrirá automáticamente

  return null;
}

export default OpenInNewWindowModal;
