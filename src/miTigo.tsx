import React, { useState, useEffect } from 'react';

const ChatwootDataRequester = () => {
  const [data, setData] = useState(null);

  const getData = () => {
    console.log("Solicitando data...");
    if (window.parent) {
      window.parent.postMessage({ type: 'chatwoot-dashboard-app:fetch-info', appId: "react-app" }, '*');
    } else {
      console.error('Parent window is not available.');
    }
  };

  useEffect(() => {
    const eventListener = (event:any) => {
      if (event.data.type === 'chatwoot-dashboard-app:data' && event.data.appId === "react-app") {
        console.log("Mensaje recibido:", event.data);
        if (isJSONValid(event.data.payload)) {
          const parsedData = JSON.parse(event.data.payload);
          setData(parsedData);
          console.log(parsedData);
        }
      }
    };

    window.addEventListener('message', eventListener);

    return () => {
      // window.removeEventListener('message', eventListener);
    };
  }, []);

  const isJSONValid = (data:any) => {
    try {
      JSON.parse(data);
      return true;
    } catch (e) {
      return false;
    }
  };

  return (
    <div>
      <button onClick={getData}>Solicitar data</button>
      {data && (
        <pre>
          {JSON.stringify(data, null, 2)}
        </pre>
      )}
    </div>
  );
};

export default ChatwootDataRequester;
