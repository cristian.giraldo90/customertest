import React, { useState, useEffect } from 'react';
import { Tabs, Tab, Box } from '@mui/material';
import Recursos from "./Recursos"; 
import MiTigo from "./miTigo"; 
import Customer from "./Customer"; 

const App = () => {
  const [data, setData] = useState({});
  const [selectedTab, setSelectedTab] = useState(0);

  const sections = [
    { title: "Información de Cliente...", content: <Customer data={data} /> },
    { title: "Recursos Externos", content: <Recursos /> },
    { title: "Mi Tigo Asesor", content: <MiTigo /> }
  ];

  const handleTabChange = (event:any, newValue:any) => {
    setSelectedTab(newValue);
    console.log(data,"dataa")
  };

  const getData = () => {
    console.log("entrooooo")
    if (window.parent && window.parent.postMessage) {
      window.parent.postMessage('chatwoot-dashboard-app:fetch-info', "https://inter-dev-co.tigocloud.net/");
    } else {
      console.error('Parent window or postMessage is not available');
    }
  };
  
  useEffect(() => {
    const eventListener = (event: MessageEvent<any>) => {
      if (isJSONValid(event.data)) {
        console.log(JSON.parse(event.data),"event.data");
        setData(JSON.parse(event.data));
      }
    };

    window.addEventListener("message", eventListener);

    return () => {
      // window.removeEventListener("message", eventListener);
    };
  }, []);

  function isJSONValid(data: string): boolean {
    try {
      JSON.parse(data);
      return true;
    } catch (e) {
      return false;
    }
  }

  return (
    <div>
      <button onClick={getData}>Solicitar data</button>
      <Box sx={{ width: '100%' }}>
        <Tabs
          value={selectedTab}
          onChange={handleTabChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          {sections.map((section, index) => (
            <Tab key={index} label={section.title} />
          ))}
        </Tabs>
        {sections.map((section, index) => (
          <TabPanel key={index} value={selectedTab} index={index}>
            {section.content}
          </TabPanel>
        ))}
      </Box>
    </div>
  );
};

const TabPanel = (props:any) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </div>
  );
};

export default App;
