import React, { useEffect, useState } from 'react';

const ChatwootDataRequester = () => {
  const [data, setData] = useState(null);

  const getData = () => {
    console.log("Solicitando data...");
    if (window.parent) {
      window.parent.postMessage('chatwoot-dashboard-app:fetch-info', "https://inter-dev-co.tigocloud.net/");
      console.log("Mensaje enviado a la ventana padre.");
    } else {
      console.error('Parent window is not available.');
    }
  };

  useEffect(() => {
    const eventListener = (event: any) => {
      console.log("Mensaje recibido:", event.data);
      if (isJSONValid(event.data)) {
        const parsedData = JSON.parse(event.data);
        setData(parsedData);
        console.log("Datos parseados:", parsedData);
      } else {
        console.warn("Datos no válidos recibidos:", event.data);
      }
    };

    window.addEventListener('message', eventListener);

    return () => {
      // window.removeEventListener('message', eventListener);
    };
  }, []);

  const isJSONValid = (data: any) => {
    try {
      JSON.parse(data);
      return true;
    } catch (e) {
      console.error("Error al parsear JSON:", e);
      return false;
    }
  };

  return (
    <div>
      <button onClick={getData}>Solicitar data</button>
      {data && (
        <pre>
          {JSON.stringify(data, null, 2)}
        </pre>
      )}
    </div>
  );
};

export default ChatwootDataRequester;
